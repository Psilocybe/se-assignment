﻿using System;

namespace SE.Simulation
{
	public class SimulationTime
	{
		public delegate void TimeTickedEventHandler(SimulationTime time);
		public event TimeTickedEventHandler TimeTicked;

		public int Tick { get; private set; }
		public int End { get { return Weeks * 7 * 24 * 60 * 60; } }
		public int Weeks { get; private set; }

		private int _WeekCounter;
		public int WeekCounter
		{
			get { return _WeekCounter; }
			set
			{
				_WeekCounter = value;
				Console.WriteLine($"Week passed({WeekCounter}/{Weeks}).");
			}
		}
		
		public int Hour
		{
			get
			{
				var s = Tick % 60;
				var m = ((Tick - s) / 60) % 60;
				return ((((Tick - s) / 60) - m) / 60) % 24;
			}
		}
		
		public DayOfWeek Day
		{
			get
			{
				var s = Tick % 60;
				var m = ((Tick - s) / 60) % 60;
				var h = ((((Tick - s) / 60) - m) / 60) % 24;
				int d = ((((((Tick - s) / 60) - m) / 60) - h) / 24) % 7;
				return (DayOfWeek)d;
			}
		}

		public SimulationTime(int weeks)
		{
			Tick = WeekCounter = 0;
			Weeks = weeks;
		}

		public void Increase(double value)
		{
			Tick += (int)value;
			TimeTicked?.Invoke(this);
		}
		public void Increase()
		{
			Increase(1);
		}

		public bool Ended()
		{
			return Tick == End;
		}
	}
}
