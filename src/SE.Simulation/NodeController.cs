﻿namespace SE.Simulation
{
	public class NodeController
	{
		public NodeController(string type, int quantity)
		{
			Type = type;
			TotalQuantity = quantity;
			IdleNodes = TotalQuantity;
		}

		public string Type { get; private set; }
		public int TotalQuantity { get; private set; }
		public int IdleNodes { get; set; }

		public bool ReserveNodes(int amount)
		{
			if (amount > 0)
			{
				IdleNodes -= amount;
				return true;
			}
			return false;
		}

		public void FreeNodes(int amount)
		{
			if (amount > 0)
			{
				IdleNodes += amount;
			}
		}
	}
}
