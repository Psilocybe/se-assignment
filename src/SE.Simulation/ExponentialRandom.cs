﻿using System;

namespace SE.Simulation
{
	public class ExponentialRandom
	{
		private Random _Random;
		public double Lambda { get; private set; }

		public ExponentialRandom(double lambda)
		{
			Lambda = lambda;
			_Random = new Random();
		}

		public ExponentialRandom(double lambda, int seed)
		{
			Lambda = lambda;
			_Random = new Random(seed);
		}

		public double NextDouble()
		{
			return Math.Log(1 - _Random.NextDouble()) / (-Lambda);
		}

		public int NextInt()
		{
			return (int)NextDouble();
		}

		public int NextInt(int min, int max)
		{
			double result = max;
			double increment = (max - min) / 6.0;
			do
			{
				result = min + (NextDouble() * increment);
			} while (result >= max);
			return (int)result;
		}
	}
}
