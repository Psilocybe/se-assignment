﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SE.Simulation
{
	public static class ConfigurationReader
	{
		public static ConfigurationSchemaType Read(string filePath)
		{
			var reader = XmlReader.Create(filePath, GetSettings());
			var serializer = new XmlSerializer(typeof(ConfigurationSchemaType));
			var configuration = serializer.Deserialize(reader) as ConfigurationSchemaType;
			if (configuration == null)
			{
				throw new XmlSchemaValidationException("Failed to load simulation configuration.");
			}
			return configuration;
		}

		private static XmlReaderSettings GetSettings()
		{
			var settings = new XmlReaderSettings();
			settings.ValidationType = ValidationType.Schema;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
			settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
			settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallback);
			settings.Schemas.Add("configuration-schema", @"Configuration\ConfigurationSchema.xsd");
			return settings;
		}

		private static void ValidationCallback(object sender, ValidationEventArgs args)
		{
			var errorWriter = Console.Error;
			switch (args.Severity)
			{
				case XmlSeverityType.Error:
					errorWriter.WriteLine($"Warning: \tSchema not found. No validation occurred. {args.Message}");
					break;
				case XmlSeverityType.Warning:
					errorWriter.WriteLine($"Validation error (L:{args.Exception.LineNumber}, P:{args.Exception.LinePosition}):\t{args.Message}");
					break;
			}
		}
	}
}
