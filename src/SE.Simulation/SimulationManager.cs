﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SE.Simulation
{
	public class SimulationManager
	{
		public SimulationManager(ConfigurationSchemaType configuration)
		{
			Time = new SimulationTime(configuration.Others.SimulationTime);
			QueueManager = new QueueManager(configuration.Queues);
			AccountingManager = new AccountingManager(configuration.ComputationalNodes);
			foreach (var group in configuration.UserGroups)
			{
				Users.AddRange(UserFactory.Produce(group));
			}
			foreach (var user in Users)
			{
				Time.TimeTicked += user.TimeTicked;
			}
		}
		public SimulationTime Time { get; private set; }
		public List<User> Users { get; set; }
		public QueueManager QueueManager { get; private set; }
		public SubmissionManager SubmissionManager { get; private set; }
		public AccountingManager AccountingManager { get; private set; }

		public void Run()
		{
			while (Time.Ended())
			{
				Time.Increase();
			}
		}

		public void GenerateReport(string reportName)
		{
			using (var stream = new FileStream(reportName, FileMode.Create))
			{
				var report = new Reporting
				{
					AverageTurnaroundTimeRatio = -1.0,
					EconomicBalance = 0.0m,
					QueueAverageWaitingTime = new Dictionary<string, int>(),
					QueuesThroughput = new Dictionary<string, int>(),
					TotalIncome = 0.0m,
					WorkingMachineHours = new Dictionary<string, int>()
				};
				report.WriteReport(stream);
			}
		}
	}
}
