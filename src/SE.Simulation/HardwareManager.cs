﻿using System.Collections.Generic;

namespace SE.Simulation
{
	public class HardwareManager
	{
		public Dictionary<string, NodeController> NodeControllers { get; private set; }

		public HardwareManager(Dictionary<string, NodeController> nodeControllers)
		{	
			NodeControllers = nodeControllers;
		}

		public bool AllocateResources(HardwareRequest requestedHardware)
		{
			if (CanAllocate(requestedHardware))
			{
				AssignNodes(requestedHardware);
				return true;
			}
			return false;
		}

		public bool CanAllocate(HardwareRequest requestedHardware)
		{
			var controller = NodeControllers[requestedHardware.Type];
			return controller.IdleNodes >= requestedHardware.NodeQuantity;
		}

		public void AssignNodes(HardwareRequest requestedHardware)
		{
			var controller = NodeControllers[requestedHardware.Type];
			controller.ReserveNodes(requestedHardware.NodeQuantity);
		}
		
		public void FreeNodes(HardwareRequest requestedHardware)
		{
			var controller = NodeControllers[requestedHardware.Type];
			controller.FreeNodes(requestedHardware.NodeQuantity);
		}
	}
}
