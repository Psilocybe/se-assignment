﻿namespace SE.Simulation
{
	public class UserJobRequestGenerator
	{
		public int CurrentId { get; private set; }
		public ExponentialRandom Random { get; private set; }
		public UserJobRequestGenerator(ExponentialRandom random)
		{
			CurrentId = 0;
			Random = random;
		}

		public Job ProduceJob(User user, SimulationTime time)
		{
			var submissionTime = (int)(Random.NextDouble() * 100000);
			var executionTime = (int)(Random.NextDouble() * 1000000);
			var nodesRequested = generateRequestedResource(user, time);
			var job = new Job()
			{
				UserId = user.Id,
				ExecutionTime = executionTime,
				NodesRequested = nodesRequested,
				SubmissionTime = time.Tick + submissionTime
			};
			return job;
		}

		public HardwareRequest generateRequestedResource(User user, SimulationTime time)
		{
			var nodes = Random.NextInt(1, 128);
			var request = new HardwareRequest("Standard", nodes);
			return request;
		}
	}
}
