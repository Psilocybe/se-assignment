﻿namespace SE.Simulation
{
	public enum JobStatus
	{
		SUBMITTED,
		PROCESSING,
		DONE
	}
}
