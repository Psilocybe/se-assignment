﻿using System;
using System.Collections.Generic;

namespace SE.Simulation
{
	public class Queue : ITimeListener
	{
		private HardwareManager _HardwareManager;

		public Queue(QueueType type, HardwareManager hardwareManager)
		{
			Type = type;
			_HardwareManager = hardwareManager;
		}

		public QueueType Type { get; set; }

		public int ProcessedJobsCounter { get; private set; }

		public List<Job> PendingJobs { get; private set; }

		public List<Job> ExecutingJobs { get; private set; }

		public bool IsAvailable(SimulationTime time)
		{
			var start = Type.AvailabilityTime.Start;
			var end = Type.AvailabilityTime.End;
			if (start.Day == time.Day)
			{
				return start.Hour <= time.Hour;
			}
			else if (end.Day == time.Day)
			{
				return end.Hour > time.Hour;
			}
			else
			{
				return start.Day < time.Day && time.Day < end.Day;
			}
		}

		public void AddJob(Job job)
		{
			PendingJobs.Add(job);
		}

		/// <summary>
		/// Cut-off time checking
		/// </summary>
		/// <param name="time">Simulation time object</param>
		/// <returns>True is queue is in cut-off time</returns>
		public bool IsQueueExtincting(SimulationTime time)
		{
			var estimatedFinishTime = time;
			estimatedFinishTime.Increase(TimeSpan.FromHours(Type.MaximumJobExecutionTime).TotalSeconds);
			return IsAvailable(estimatedFinishTime);
		}

		public void TryToMoveJobsBetweenAreas(SimulationTime time)
		{
			var toDelete = new List<Job>();
			foreach (var job in PendingJobs)
			{
				if (_HardwareManager.AllocateResources(job.NodesRequested))
				{
					job.StartTime = time.Tick;
					ExecutingJobs.Add(job);
				}
			}
			foreach (var job in toDelete)
			{
				PendingJobs.Remove(job);
			}
		}

		public void ProcessFinishedJobs(SimulationTime time)
		{
			var toDelete = new List<Job>();
			foreach (var job in ExecutingJobs)
			{
				if (job.EndTime >= time.Tick)
				{
					_HardwareManager.FreeNodes(job.NodesRequested);
					job.EndTime = time.Tick;
					toDelete.Add(job);
				}
			}
			foreach (var job in toDelete)
			{
				ExecutingJobs.Remove(job);
			}
		}
		public void TimeTicked(SimulationTime time)
		{
			if (IsAvailable(time))
			{
				ProcessFinishedJobs(time);
				if (!IsQueueExtincting(time))
					TryToMoveJobsBetweenAreas(time);
			}
		}
	}
}
