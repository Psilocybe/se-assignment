﻿using System;

namespace SE.Simulation
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				var options = new CommandLineOptions();
				if (CommandLine.Parser.Default.ParseArguments(args, options))
				{
					var configuration = ConfigurationReader.Read(options.ConfigurationPath);
					var simulataion = new SimulationManager(configuration);
					simulataion.Run();
					simulataion.GenerateReport(options.ReportName);
				}
			}
			catch (Exception e)
			{
				var errorWriter = Console.Error;
				errorWriter.WriteLine(e.Message);
			}
		}
	}
}
