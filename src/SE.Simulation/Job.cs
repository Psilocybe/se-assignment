﻿namespace SE.Simulation
{
	public class Job
	{
		public int UserId { get; set; }

		public int? SubmissionTime { get; set; }

		public int? StartTime { get; set; }

		public int? EndTime { get; set; }

		public int ExecutionTime { get; set; }

		public HardwareRequest NodesRequested { get; set; }

		public JobStatus Status
		{
			get
			{
				if (EndTime.HasValue) return JobStatus.DONE;
				if (StartTime.HasValue) return JobStatus.PROCESSING;
				return JobStatus.SUBMITTED;
			}
		}
	}
}
