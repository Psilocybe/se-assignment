﻿using System.Collections.Generic;

namespace SE.Simulation
{
	public static class UserFactory
	{
		private static int IdCounter = 0;
		static public List<User> Produce(UserGroupType group)
		{
			var users = new List<User>();
			for (int i = 0; i < group.Quantity; i++)
			{
				var user = new User(IdCounter++, group.Budget, group.ConcurentJobsLimit, group.JobSubmissionRateParameter);
				users.Add(user);
			}
			return users;
		}
	}
}
