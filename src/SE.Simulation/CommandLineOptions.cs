﻿using CommandLine;
using CommandLine.Text;

namespace SE.Simulation
{
	public class CommandLineOptions
	{
		[Option('c', "config", Required = true, HelpText = "Configuration file path.")]
		public string ConfigurationPath { get; set; }

		[Option('o', "out", DefaultValue = "report.txt", HelpText = "Output report file name.")]
		public string ReportName { get; set; }

		[ParserState]
		public IParserState LastParserState { get; set; }

		[HelpOption]
		public string GetHelp()
		{
			return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}
	}
}
