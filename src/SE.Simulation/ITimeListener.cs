﻿namespace SE.Simulation
{
	public interface ITimeListener
	{
		void TimeTicked(SimulationTime time);
	}
}
