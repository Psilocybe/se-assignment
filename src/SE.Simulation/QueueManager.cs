﻿using System.Collections.Generic;

namespace SE.Simulation
{
	public class QueueManager
	{
		public List<Queue> Queues { get; set; }

		public QueueManager(QueueType[] queuesConfig)
		{
			Queues = new List<Queue>();
			foreach (var queue in queuesConfig)
			{
				var hardwareManager = new HardwareManager(new Dictionary<string, NodeController>());
				Queues.Add(new Queue(queue, hardwareManager));
			}
		}
	}
}
