﻿using System;

namespace SE.Simulation
{
	public class User : ITimeListener
	{
		public User(int id, decimal budget, int jobLimit, double rateParameter)
		{
			Id = id;
			Budget = budget;
			ConcurrentJobsLimit = jobLimit;
			JobSubmissionRateParameter = rateParameter;
			NextRequest = null;
		}
		
		public int Id { get; private set; }

		public decimal Budget { get; set; }

		public int ConcurrentJobsLimit { get; set; }

		public double JobSubmissionRateParameter { get; set; }

		public Job NextRequest { get; set; }

		public void TimeTicked(SimulationTime time)
		{
			if (NextRequest != null)
			{
				if (NextRequest.SubmissionTime >= time.Tick)
				{
					
				}
			}
			else
			{
				
			}
		}
	}
}
