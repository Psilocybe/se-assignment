﻿namespace SE.Simulation
{
	public class HardwareRequest
	{
		public HardwareRequest(string type, int quantity)
		{
			Type = type;
			NodeQuantity = quantity;
		}

		public string Type { get; set; }
		public int NodeQuantity { get; set; }
	}
}
