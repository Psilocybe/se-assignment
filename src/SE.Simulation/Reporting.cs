﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SE.Simulation
{
	public class Reporting
	{
		public Dictionary<string, int> WorkingMachineHours { get; set; }
		public Dictionary<string, int> QueuesThroughput { get; set; }
		public Dictionary<string, int> QueueAverageWaitingTime { get; set; }
		public double AverageTurnaroundTimeRatio { get; set; }
		public decimal TotalIncome { get; set; }
		public decimal EconomicBalance { get; set; }
		
		public void WriteReport(Stream stream)
		{
			using (var writer = new StreamWriter(stream))
			{
				writer.WriteLine("===================");
				writer.WriteLine("MACHINES STATISTICS");
				writer.WriteLine("===================");
				int total = 0;
				WorkingMachineHours.Aggregate(total, (sum, kvp) => sum += kvp.Value);
				writer.WriteLine($"Total working machine hours: {total}");
				foreach (var machine in WorkingMachineHours)
				{
					writer.WriteLine($"Working machine hours in {machine.Key}: {machine.Value}");
				}
				writer.WriteLine();
				writer.WriteLine("==============");
				writer.WriteLine("FINANCIAL DATA");
				writer.WriteLine("==============");
				writer.WriteLine($"Income: {TotalIncome}");
				writer.WriteLine($"Economic balance: {EconomicBalance}");
				writer.WriteLine($"Incurred costs: {TotalIncome - EconomicBalance}");

				writer.WriteLine();
				writer.WriteLine("================");
				writer.WriteLine("QUEUE STATISTICS");
				writer.WriteLine("================");
				writer.WriteLine($"Average turnaround time ratio: {AverageTurnaroundTimeRatio}");
				foreach (var queue in QueueAverageWaitingTime)
				{
					writer.WriteLine($"Average waiting time in {queue.Key} queue: {queue.Value}");
				}
				foreach (var queue in QueuesThroughput)
				{
					writer.WriteLine($"Throughput of {queue.Key} queue: {queue.Value}");
				}
			}
		}
	}
}
