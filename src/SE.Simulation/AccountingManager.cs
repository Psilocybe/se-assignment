﻿using System.Linq;

namespace SE.Simulation
{
	public class AccountingManager
	{
		public ComputationNodeType[] Nodes { get; private set; }

		public AccountingManager(ComputationNodeType[] nodes)
		{
			Nodes = nodes;
		}

		public decimal CalculatePrice(Job job)
		{
			decimal price = 0;
			var nodePerHourCost = Nodes.First(n => n.Name == job.NodesRequested.Type).UserPrice;
			price += job.NodesRequested.NodeQuantity * nodePerHourCost * (job.ExecutionTime / 60 / 60);
			return price;
		}
	}
}
