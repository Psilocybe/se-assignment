﻿using NUnit.Framework;
using System.Collections.Generic;

namespace SE.Simulation.Test
{
	[TestFixture]
	public class HardwareManagerTest
	{
		private HardwareManager _Manager;

		[SetUp]
		public void SetUp()
		{
			_Manager = new HardwareManager(
				new Dictionary<string, NodeController>()
				{
					{ "Standard", new NodeController("Standard", 100) }
				});
		}

		[Test]
		public void ShouldAllocateResources()
		{
			var request = new HardwareRequest("Standard", 10);
			bool success = _Manager.AllocateResources(request);
			Assert.AreEqual(true, success);
		}

		[Test]
		public void ShouldNotAllocateResources()
		{
			var request = new HardwareRequest("Standard", 1001);
			bool fail = _Manager.AllocateResources(request);
			Assert.AreEqual(false, fail);
		}

		[Test]
		public void CanAllocateTest()
		{
			bool success = _Manager.CanAllocate(new HardwareRequest("Standard", 5));
			Assert.AreEqual(true, success);

			bool fail = _Manager.CanAllocate(new HardwareRequest("Standard", 200));
			Assert.AreEqual(false, fail);
		}

		[Test]
		public void CheckAllocationAndDeallocation()
		{
			int baseQuantity = _Manager.NodeControllers["Standard"].TotalQuantity;
			var request = new HardwareRequest("Standard", 50);
			_Manager.AllocateResources(request);
			Assert.AreEqual(baseQuantity - 50, _Manager.NodeControllers["Standard"].TotalQuantity - 50);

			_Manager.FreeNodes(request);
			Assert.AreEqual(baseQuantity, _Manager.NodeControllers["Standard"].TotalQuantity);
		}
	}
}
