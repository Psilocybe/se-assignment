﻿using NUnit.Framework;
using System.Collections.Generic;

namespace SE.Simulation.Test
{
	[TestFixture]
	public class AccountingManagerTest
	{
		const string TestNodeType = "Standard";
		private AccountingManager _Manager;
		private List<ComputationNodeType> _Nodes;

		[SetUp]
		public void SetUp()
		{
			_Nodes = new List<ComputationNodeType>
			{
				new ComputationNodeType
				{
					Name = TestNodeType,
					MaintenanceCost = 1.0m,
					UserPrice = 2.0m,
					Quantity = 100
				}
			};
			_Manager = new AccountingManager(_Nodes.ToArray());
		}

		[Test]
		public void ShouldCalculateRequestPrice()
		{
			var job = new Job
			{
				ExecutionTime = 6 * 60 * 60, //6h
				NodesRequested = new HardwareRequest(TestNodeType, 10) //10 nodes
			};
			var price = _Manager.CalculatePrice(job);
			Assert.AreEqual(120, price); //10 * 6[h] * 2[1/h]
		}
	}
}
