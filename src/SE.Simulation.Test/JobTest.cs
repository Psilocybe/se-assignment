﻿using System;
using NUnit.Framework;

namespace SE.Simulation.Test
{
	[TestFixture]
	public class JobTest
	{
		[Test]
		public void ShouldBeSubmitted()
		{
			var job = new Job();
			job.SubmissionTime = 1;
			Assert.AreEqual(job.Status, JobStatus.SUBMITTED);
		}

		[Test]
		public void ShouldBeProcessing()
		{
			var job = new Job();
			job.SubmissionTime = 1;
			job.StartTime = 2;
			Assert.AreEqual(job.Status, JobStatus.PROCESSING);
		}

		[Test]
		public void ShouldBeDone()
		{
			var job = new Job();
			job.SubmissionTime = 1;
			job.StartTime = 2;
			job.EndTime = 3;
			Assert.AreEqual(job.Status, JobStatus.DONE);
		}
	}
}
