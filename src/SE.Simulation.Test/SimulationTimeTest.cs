﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SE.Simulation.Test
{
	[TestFixture]
	public class SimulationTimeTest
	{
		private SimulationTime time;

		[SetUp]
		public void SetUp()
		{
			time = new SimulationTime(1);
		}

		[Test]
		public void ShouldBeZeroInitialized()
		{
			Assert.AreEqual(time.Day, DayOfWeek.Monday);
			Assert.AreEqual(time.Tick, 0);
			Assert.AreEqual(time.Hour, 0);
			Assert.AreEqual(time.WeekCounter, 0);
		}
		
		[Test]
		public void ShouldRaiseEventAfterIncrement()
		{
			List<SimulationTime> receivedEvents = new List<SimulationTime>();
			time.TimeTicked += (SimulationTime time) =>
			{
				receivedEvents.Add(time);
			};
			time.Increase();
			Assert.AreEqual(1, receivedEvents.Count);
		}

		[Test]
		public void ShouldBeProperTimeAndDay()
		{
			int seconds = 3 * 24 * 60 * 60 + // 3 days
							  12 * 60 * 60;  // 12 hours
			time.Increase(seconds);
			Assert.AreEqual(DayOfWeek.Thursday, time.Day);
			Assert.AreEqual(12, time.Hour);
		}

		[Test]
		public void ShouldSwitchToEndState()
		{
			int end = time.End;
			for (int i = 0; i < end; i++)
			{
				time.Increase();
			}
			Assert.AreEqual(true, time.Ended());
		}
	}
}
