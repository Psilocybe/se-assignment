﻿using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace SE.Simulation.Test
{
	[TestFixture]
	public class QueueTest
	{
		private QueueType typeMock;
		private HardwareManager hardwareManagerMock;

		[SetUp]
		public void SetUp()
		{
			typeMock = new QueueType
			{
				AvailabilityTime = new QueueTypeAvailabilityTime
				{
					Start = new QueueTypeAvailabilityTimeStart
					{
						Day = DayOfWeek.Monday,
						Hour = 9
					},
					End = new QueueTypeAvailabilityTimeEnd
					{
						Day = DayOfWeek.Friday,
						Hour = 17
					}
				}
			};
			hardwareManagerMock = new HardwareManager(new Dictionary<string, NodeController>());
		}

		[Test]
		public void ShouldBeAvailable()
		{
			var time = new SimulationTime(1);
			var seconds = TimeSpan.FromDays((int)typeMock.AvailabilityTime.Start.Day).TotalSeconds +
				TimeSpan.FromHours(typeMock.AvailabilityTime.Start.Hour).TotalSeconds;
			time.Increase(seconds);
						
			var queue = new Queue(typeMock, hardwareManagerMock);
			Assert.AreEqual(true, queue.IsAvailable(time));
		}

		[Test]
		public void ShouldNotBeAvailable()
		{
			var time = new SimulationTime(1);
			var seconds = TimeSpan.FromDays((int)typeMock.AvailabilityTime.End.Day).TotalSeconds +
				TimeSpan.FromHours(typeMock.AvailabilityTime.End.Hour).TotalSeconds;
			time.Increase(seconds);

			var queue = new Queue(typeMock, hardwareManagerMock);
			Assert.AreEqual(false, queue.IsAvailable(time));
		}
	}
}
