\section{Simulation Configuration}

\textsc{Priority:} Medium

\subsection*{Description}
Application user may specify simulation parameters by filling template configuration file.
System will provide user with basic validation of these parameters, such as parameters consistency, range control or checking for usage of unknown options.
Person responsible for choosing values of configuration parameters can check all available values within a template file. Diagram \ref{fig:uc-configuration} shows required configuration options, available to set by using configuration file.

\begin{figure}[!htbp]
	\includegraphics[height=10cm]{uc-config}
	\caption{Use case diagram for simulation configuration.}
	\label{fig:uc-configuration}
\end{figure}

\pagebreak

\subsection*{Stimulus-Response Sequences}
\stimresp
{User requests all available program parameters.}
{Application prints User Manual in current console window.}

\medskip

\stimresp
{User specifies simulation parameters.}
{System queries user for configuration file that is compatible with template file.}

\medskip

\stimresp
{User requests configuration validation.}
{If parameters are consistent and logical, system will inform user about that and run simulation. Otherwise user will receive a list of errors in configuration.}

\subsection*{Functional Requirements}

\begin{functional}{Show User Manual}{Medium}{None}
	\label{fn:user-manual}
	\term{DESC}{User should be able to invoke User Manual with a \texttt{\emph{-h / --help}} argument passed to a program.}
	\term{RAT}{In order for a user to know how to configure and run simulation.}
\end{functional}

\begin{functional}{Configuration File Location}{Medium}{None}
	\label{fn:configuration-location}
	\term{DESC}{User should be able to specify configuration file location with a \texttt{\emph{-c <file\char`_path> / --config <file\char`_path>}} argument passed to a program.}
	\term{RAT}{In order for a user to pass a simulation configuration file.}
\end{functional}

\begin{functional}{Output Report Name}{Medium}{None}
	\label{fn:output-name}
	\term{DESC}{User should be able to specify output report file name with a \texttt{\emph{-o <file\char`_name>/ --out <file\char`_name>}} argument passed to a program.}
	\term{RAT}{In order for a user to specify output file name.}
\end{functional}

\begin{functional}{Configuration}{High}{\ref{fn:configuration-nodes}, \ref{fn:configuration-queues}, \ref{fn:configuration-user-groups}, \ref{fn:configuration-simulation-time}}
	\label{fn:configuration}
	\term{DESC}{Software should allow an easy and repeatable way to configure simulation.}
	\term{RAT}{In order to set up simulation.}
\end{functional}

\begin{functional}{Configuration -- Computational Nodes}{High}{None}
	\label{fn:configuration-nodes}
	\term{DESC}{Application shall provide functionality to set up computational node types, their quantity, maintenance cost and price for machine-hour.}
	\term{RAT}{Simulating different sets of hardware.}
\end{functional}

\begin{functional}{Configuration -- \gls{DRBG} Seed}{High}{None}
	\term{DESC}{Application shall provide functionality to set up the \gls{DRBG} seed.}
	\term{RAT}{In order to make possible the reproduction of the output report between different simulation runs.}
\end{functional}

\begin{functional}{Configuration -- Queues}{High}{None}
	\label{fn:configuration-queues}
	\term{DESC}{
		User should be able to set up many queues for processing different classes of jobs. Their configuration have to include:
		\begin{itemize}
			\item type and description;
			\item maximum job execution time;
			\item availability time;
			\item price factor;
			\item amount of reserved hardware resources.
		\end{itemize}
		Availability time should be specified as weekly working time with accuracy to the minute.
	}
	\term{RAT}{In order to examine income from queues of various execution times and quantity of reserved resources.}
\end{functional}

\begin{functional}{Configuration -- User Groups}{High}{None}
	\label{fn:configuration-user-groups}
	\term{DESC}{Application should provide functionality to specify user groups with following parameters:
	\begin{itemize}
		\item quantity of users in group;
		\item budget range;
		\item maximum number of concurrent jobs per user;
		\item maximum resources (in cores) available per user;
		\item maximum total number of cores simultaneously utilized by user.
	\end{itemize}
	All parameters of the above can be treated as positive integer values.
	}
	\term{RAT}{In order to define the target group of the system users and their quotas.}
\end{functional}

\begin{functional}{Configuration -- Job Submission Rate}{Low}{None}
	\label{fn:configuration-job-submission-rate}
	\term{DESC}{Application should provide functionality to specify rate of submitted jobs (see \ref{fn:users-job-requests-interval}).}
	\term{RAT}{In order to describe the distribution of the jobs submitted to the system.}
\end{functional}

\begin{functional}{Configuration -- Simulation Time}{Medium}{None}
	\label{fn:configuration-simulation-time}
	\term{DESC}{User should be able to configure time of simulation in number of weeks.}
	\term{RAT}{In order to specify how long should simulation run.}
\end{functional}

\begin{functional}{Configuration Validation}{Medium}{\ref{fn:configuration}, \ref{fn:config-validation-nodes}, \ref{fn:config-validation-queues}, \ref{fn:config-validation-user-groups}}
	\label{fn:config-validation}
	\term{DESC}{User should be informed whether configuration file is valid.}
	\term{RAT}{It is intended to inform user about illogical or inconsistent configuration values.}
\end{functional}

\newpage

\begin{functional}{Configuration Validation -- Computational Nodes}{Low}{\ref{fn:configuration-nodes}}
	\label{fn:config-validation-nodes}
	\term{DESC}{Application should validate each given node type and check if maintenance cost is smaller than price. Additionally, if users did not specify any computational nodes, system should notice that.}
	\term{RAT}{Correct configuration of computational nodes.}
\end{functional}

\begin{functional}{Configuration Validation -- Queues}{Medium}{\ref{fn:configuration-queues}}
	\label{fn:config-validation-queues}
	\term{DESC}{Application should, if given queues configuration specifies logical availability time and that reserved resources for each queue sum up to 100\%. Configuration shall have  specified at least one queue. Price factor has to be positive value.}
	\term{RAT}{Correct configuration of job queues.}
\end{functional}

\begin{functional}{Configuration Validation -- User Groups}{Medium}{\ref{fn:configuration-user-groups}}
	\label{fn:config-validation-user-groups}
	\term{DESC}{User has to configure at least one user group for simulation to work properly. Quantity of users in each group has to be greater than 0. Budget ranges, and all resources limits have to be positive non-zero values.}
	\term{RAT}{Correct configuration of user groups and their limitations.}
\end{functional}

\begin{functional}{Configuration Validation -- Simulation Time}{Medium}{\ref{fn:configuration-simulation-time}}
	\label{fn:config-validation-time}
	\term{DESC}{Time should be given as an positive integer number of weeks. Total number of weeks should not exceed 2 years (max 105 weeks).}
	\term{RAT}{Correct configuration of simulation time.}
\end{functional}

\begin{functional}{Displaying Configuration Errors and Warnings}{Low}{\ref{fn:config-validation}}
	\label{fn:config-error-displaying}
	\term{DESC}{If software detects errors or minor inconsistencies in configuration, it should provide user with information how to fix them. Error should contain name of incorrect parameter and current value; additionally available or correct values can be provided.}
	\term{RAT}{Providing user with validation information.}
	\term{SCALE}{User-friendliness and utility of validation informations.}
	\term{METER}{Detail level of error and warning messages.}
	\term{WISH}{Provide user with well formatted list of all configuration errors and warnings and available correct values.}
	\term{PLAN}{Software checks configuration line by line and stops at first major error and prints it's details.}
	\term{MUST}{Application have to inform at least about which parameter is incorrect.}
\end{functional}
\newpage
