\section{Imitating Supercomputer Management Platform}

\textsc{Priority:} High

\subsection*{Description}
User wants to have simulation software for \gls{hpc} platform, that will provide him with financial data. User requires, that the application will be able to fake different user classes and imitate various queues for their requests. Users wants to have possibility to specify different computer node types, define their maintenance cost per unit of time and usage price.
Diagram \ref{fig:uc-app} shows main actions between User and software.

\begin{figure}[!htbp]
	\includegraphics[height=10cm]{uc-app}
	\caption{Use case diagram for main application features.}
	\label{fig:uc-app}
\end{figure}

\subsection*{Stimulus-Response Sequences}
\stimresp
{Application user configures different types of machines to simulate.}
{Software expects user to provide information about simulated node type, pricing and description.}

\medskip

\stimresp
{User sets up different queue types with various maximum execution time and pricing.}
{Queue Manager instantiates and monitors queue of each specified by user type, that will wait for in-simulation user job requests.}

\medskip

\stimresp
{User sets up various groups of in-simulation system users, that spawn all of job categories up to specified budget to queues.}
{User Job Request Generator spawns number of in-simulation users, that will generate job requests.}

\medskip

\stimresp
{User runs simulation.}
{After parsing configuration file and setting up submodules, application begins simulation.}

\medskip

\stimresp
{User tracks simulation progress.}
{During simulation, software logs the most important simulation events in console window. Program shows also estimated simulation end time and/or remaining users budget.}

\subsection*{Functional Requirements}
\begin{functional}{Simulating Users}{High}{\ref{fn:user-jobs-variety}, \ref{fn:users-budget-utilization}, \ref{fn:users-job-requests-interval}}
	\label{fn:user}
	\term{DESC}
	{
		In-simulation users should be considered just as job requests producers, with limited budged.
		Their submissions should contain jobs from every category.
		Users will be divided into groups, each having different budget limits and varying chances to produce certain kind of job.
		It is assumed, that a single user can run only one job simultaneously in whole system.
	}
	\term{RAT}{In intention to fake real system users.}
\end{functional}

\begin{functional}{Simulating Users -- Jobs Variety}{High}{None}
	\label{fn:user-jobs-variety}
	\term{DESC}
	{
		Depending on the configuration of a group that user belongs to, he should produce jobs of each kind with probability specified for his group.
		Algorithm responsible for selection of job's class should be pseudorandom, in order to achieve it's testability and repeatability.
		Each job should have specified following parameters:
		\begin{itemize}
			\item amount of computing nodes of certain type and number of cores;
			\item estimated execution time.
		\end{itemize}
	}
	\term{RAT}{In order to ensure variety and testability of in-simulation users products.}
\end{functional}

\begin{functional}{Simulating Users -- Budget Utilization}{High}{None}
	\label{fn:users-budget-utilization}
	\term{DESC}
	{
		Algorithm determining should take into account user's available budget and maximize it's utilization.
		It is assumed that users will try to utilize every available funds they have in credit.
		User cannot request a job, that will exceed his budget.
	}
	\term{RAT}{In intention to model in-simulation users' expenditures.}
\end{functional}

\begin{functional}{Simulating Users -- Job Requests Interval}{Medium}{None}
	\label{fn:users-job-requests-interval}
	\term{DESC}
	{
		Time between two consecutive job requests submitted by single user should be modelled by en exponential probability function. Density of the probability function is given in equation \ref{eq:job-requests-density}. This probability can be changed across all user groups by specifying different $\lambda$ factor.
		\begin{equation}
			\label{eq:job-requests-density}
			f(x) = \lambda\mathrm{e}^{-\lambda x}
		\end{equation}
		where
		\begin{itemize}
			\item $\lambda$ is the rate parameter,
			\item $x$ is the time of simulation, $x>0$
			\item $\mathrm{e}$ is the base of the natural logarithm.
		\end{itemize}
	}
	\term{RAT}{In order to establish users job submission frequency.}
\end{functional}

\newpage

\begin{functional}{Job Status}{High}{None}
	\label{fn:job-status}
	\term{DESC}
	{
		Each job should have assigned status and time between each status change should be measured -- especially time between submission and start of processing.
		Minimal set of statuses should contain:
		\begin{enumerate}
			\item \texttt{\textbf{SUBMITTED}}
			\item \texttt{\textbf{PROCESSING}}
			\item \texttt{\texttt\textbf{DONE}}
		\end{enumerate}
	}
	\term{RAT}{In order to provide a way to calculate statistics of jobs.}
\end{functional}

\begin{functional}{Tracking Submissions}{High}{\ref{fn:job-status}}
	\label{fn:tracking-submissions}
	\term{DESC}
	{
		Application should track each and every job from it's production by a user, through queue, until it's processed and finished.
	}
	\term{RAT}{In order to track each job and measure it's \gls{turnaround-time-ratio} and waiting time.}
\end{functional}

\begin{functional}{Simulating Queues}{High}{\ref{fn:queues-statistics}, \ref{fn:queues-jobs-ascription}, \ref{fn:scheduler}}
	\label{fn:queues}
	\term{DESC}
	{
		System policy constraints state that system should handle at least four different job queues. Details are given in Table \ref{tab:queues-details}.
		Queue manager should match available hardware resources to pending jobs. Each queue should implement the same scheduler algorithm (see \ref{fn:scheduler}).
	}
	\term{RAT}{In order to describe queues' specification and responsibilities.}
\end{functional}

\begin{table}[!htbp]
	\centering
	\caption{Job queues specification.}
	\label{tab:queues-details}
	\begin{tabular}{@{}ccccp{0.3\textwidth}@{}}
		\toprule
		Name & \specialcell{Resources \\per job (max)} & \specialcell{Max \\execution time} & \specialcell{Reserved \\resources} & Notes\\
		\midrule
		\specialcell{Short\\Interactive} & 2 nodes    & 1 hour   & $\approx 10\%$ & Available 0900am Monday - 0500pm Friday\\
		Medium                           & 10\% nodes & 8 hours  & $\approx 30\%$ & Available 0900am Monday - 0500pm Friday\\
		Large                            & 50\% nodes & 16 hours & $\approx 60\%$ & Available 0900am Monday - 0500pm Friday\\
		Huge                             & all nodes      & 64 hours & $100\%$ & Available 0500pm Friday - 0900am Monday\\
		\bottomrule
	\end{tabular}
\end{table}

\newpage

\begin{functional}{Simulating Queues -- Statistics}{High}{None}
	\label{fn:queues-statistics}
	\term{DESC}{Application should collect data about all jobs that are pending, processing and executed in each queue.}
	\term{RAT}{In order to provide software user with queues statistics.}
\end{functional}

\begin{functional}{Simulating Queues -- Jobs Ascription}{High}{None}
	\label{fn:queues-jobs-ascription}
	\term{DESC}
	{
		Program have to assign each job produced by in-simulation users to proper queue.
		This ascription should be based on requested resources that job requires.
	}
	\term{RAT}{In intention to determine responsibilities of queuing system.}
\end{functional}

\begin{functional}{Simulating Queues -- Cut-Off Time}{High}{None}
	\label{fn:queues-cut-off-time}
	\term{DESC}
	{
		Simulated queues should have a cut-off time. Within that time no new jobs should start execution if their estimated completion time will exceed availability of the queue for the users (see \ref{tab:queues-details}).
	}
	\term{RAT}{In order to achieve good queuing system and preserve constant intervals of queues availability.}
\end{functional}

\begin{functional}{Simulating Hardware}{High}{None}
	\label{fn:hardware}
	\term{DESC}
	{
		Application shall not allow for exceeding the limit of nodes and cores per node usage configured by user. It should monitor machines usage and collect data about idle and working time of each node.
	}
	\term{RAT}{The way hardware and what parts of it are imitated inside simulation.}
\end{functional}

\begin{functional}{Accounting}{High}{None}
	\label{fn:accounting}
	\term{DESC}
	{
		Program should keep under constant review the budget of in-simulation users, collect statistical data about on what they spent credits and do not allow exceeding of their budget. Software also has to measure running machines operational costs.
	}
	\term{RAT}{In order to provide software user with financial data.}
\end{functional}

\begin{functional}{Scheduler}{High}{None}
	\label{fn:scheduler}
	\term{DESC}
	{
		Simulation should have a scheduler that is a simple first come first serve algorithm.
		It should queue jobs in the same order as they are submitted.
	}
	\term{RAT}{In intention to define a scheduler algorithm.}
\end{functional}

\newpage

\begin{functional}{Tracking Simulation Progress}{Low}{None}
	\label{fn:tracking-simulation-progress}
	\term{DESC}
	{
		Application should track simulation progress and it's remaining time, i.e. total remaining users budget, total number of processed jobs, incurred costs and income. This informations should be presented to the user during the whole simulation. 
	}
	\term{RAT}{In intention to provide user with basic data during simulation and it's progress.}
	\term{SCALE}{User awareness of simulation progress.}
	\term{METER}{Detail level of progress information provided to user.}
	\term{WISH}{Real-time data about all major simulation outputs and remaining time.}
	\term{PLAN}{Weekly report (in-simulation time) containing main simulation outputs.}
	\term{MUST}{User has to be notified after completion of each week of in-simulation time.}
\end{functional}

\begin{functional}{Simulation Time}{High}{None}
	\label{fn:simulation-time}
	\term{DESC}
	{
		Application should implement a counter, that will act as time indicator for whole simulation.
		It is assumed, that with each incrementation of that counter program should simulate next time frame of management system, i.e
		users should produce new jobs, queues should process next chunk of jobs, etc.
	}
	\term{RAT}{In order to define time for simulation.}
	\term{SCALE}{Mapping of in-simulation counter to real system time.}
	\term{WISH}{Counter step represents 1 second of real time.}
	\term{MUST}{Maximal counter step should not be greater than 1 minute.}
\end{functional}

\begin{functional}{Simulation Stop Condition}{High}{\ref{fn:configuration-simulation-time}}
	\label{fn:simulation-stop-condition}
	\term{DESC}
	{
		Program shall stop simulation when either one of those conditions is true:
		\begin{enumerate}
			\item Simulation configured time ran out.
			\item All of the in-simulation users ran out of budget, so they are incapable of submitting any kind of job.
		\end{enumerate}
	}
	\term{RAT}{In order to determine when the simulation should end.}
\end{functional}
