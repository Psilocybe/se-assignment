\chapter{Introduction}

\section{Scope}
	The purpose of this MTP is to document the actual management and scope of the total test effort of the \gls{projname}, which is a simulating tool for HPC management software described in SRS paper [1]. This test plan will cover following objectives:
	\begin{itemize}
		\item Describe the unit, integration and system tests as well as code coverage.
		\item List the recommended and required testing areas.
		\item Describe the strategies and approaches used to test the software.
		\item List and present the form of the deliverable documents of the test activities.
	\end{itemize}
		
	Software being tested is designed for the purposes of simulating an HPC management system ordered by the Cranfield University IT Department. The main reason for which this project will be used, is to establish good accounting practices for real HPC system. 
	
	The interfaces between the following internal parts of application will be tested:
	\begin{itemize}
		\item User Job Request Generator,
		\item Submission Manager,
		\item Queue Manager,
		\item Accounting Module,
		\item Hardware Manager.
	\end{itemize}

	There are no external dependencies to test.
	
	Application is being developed under agile methodologies, therefore after completing each iteration tests should be rerunned and results should be included into the test log.

\section{References}
\subsection*{External references}
\begin{enumerate}
	\item \emph{IEEE Std. 829-2008 - IEEE Standard for Software and System Test Documentation}.
	IEEE Computer Society, 2008.
\end{enumerate}

\subsection*{Internal references}
\begin{enumerate}
	\item \emph{Software Requirements Specification for Supercomputer Job Control, Submission and Accounting Simulation}. Szostek, Bartłomiej. Cranfield University, 2017.
\end{enumerate}

\section{System overview and key features}
	Main system features are as follows:
	\begin{enumerate}
		\item Simulation Configuration,
		\item Imitating Supercomputer Management Platform,
		\item Simulation Report Generation.
	\end{enumerate}
	System overview is described in details in SRS document [1].

\section{Test overview}
	\begin{wrapfigure}{R}{0.5\textwidth}
		\begin{center}
			\includegraphics[width=0.48\textwidth]{tdd}
		\end{center}
		\caption{Relationship between software testing and development.}
		\label{fig:tdd}
	\end{wrapfigure}

	This section describes test organization, tools and techniques necessary to perform product testing. A primary objective of testing software is to assure that the system meets the full
	requirements. At the end of the project development cycle, i.e. after acceptance tests, the user should find that the software has met or exceeded all of their expectations as detailed in the SRS document.

\subsection{Organization}
	Test processes are crucial to development of software. Low level unit testing should be done using \gls{tdd} techniques, as shown in \ref{fig:tdd}. This document will lately describe a set of regression tests, that should be run each time before submitting new chunk of code. Test processes should not affect defined requirements, because of the simplicity of application under development. 
	Application will be tested internally, without the help of external organization. \Gls{qa} will take place at the end of each agile cycle. \Gls{qa} team members will prepare end-user test scenarios and acceptance tests, in order to prove fulfilment of terms by software.
	
\subsection{Master test schedule}
	Due to low the complexity of the application and agile methodology, code development and testing should be executed in parallel. Developers and testers ought to provide daily feedback to each other.
	
	Main project milestones can be distinguished by main system features in following order:
	\begin{enumerate}
		\item Application--User Interface for setting up simulation: configuration file template, validation, user manual.
		\item Running Simulation: simulating HPC system users, queues, hardware and accounting module, progress visualization.
		\item Final Report Generation: format of the report, calculating statistics.
	\end{enumerate}

\subsection{Integrity level scheme}
\label{sec:integrity}
	As stated in SRS document, this application is a self-contained software used only as a simulation tool. Results provided by this software may cause negligible financial loose for a client. Hence, using integrity level scheme introduced in IEEE 829-2008 Standard, this software has been classified as having 1 integrity level (lowest). Therefore, following documents will be provided in this test plan:
	\begin{itemize}
		\item Master Test Plan
		\item Level Test Plan (Component Integration, System)
		\item Level Test Design (Component Integration, System)
		\item Level Test Case (Component Integration, System)
	\end{itemize}
	
	Apart from the standard, Master Test Plan has been added to the list of documents, for better management and understanding of test plan. Following documents: Level Test Procedure, Anomaly Report, Level Test Report and Level Test Log will not be provided. Moreover, all LTP and LTD papers will be merged into one Level Test Design document.

\subsection{Resources summary}
	This section presents the recommended resources for testing the \gls{projname}.
	The following table \ref{tab:system-resources} sets forth the system and hardware resources for testing the software.
	\begin{table}[!htbp]
		\centering
		\caption{System and hardware resources.}
		\label{tab:system-resources}
		\begin{tabular}{lcp{0.6\linewidth}}
			\toprule
			Resource Name & Quantity & Notes \\
			\midrule
			Test Repository & 1 & Server to keep all tests and their history. \\
			\midrule
			Test Development PC & 1 & Machine used to develop tests, with access to Test Repository server. \\
			\midrule
			Visual Studio 2015 & 1 & Licence for development environment for software engineers. \\
			\bottomrule
		\end{tabular}
	\end{table}
	
\pagebreak
	
\subsection{Responsibilities}
	Table \ref{tab:human-resources} shows the staff and their responsibilities for the test of the application.
	\begin{table}[!htbp]
		\centering
		\caption{Human resources.}
		\label{tab:human-resources}
		\begin{tabular}{llp{0.6\linewidth}}
			\toprule
			Role & Workers & Responsibilities and Notes \\
			\midrule
			Test Manager  & Szostek B. & Provides management oversight. Duties:\\
			&& \tabitem Provide technical direction.\\
			&& \tabitem Acquire needed resources.\\
			\midrule
			Test Designer & Szostek B. & 
			Identifies, prioritizes, and implements test cases. Duties:\\
			&& \tabitem Generate Test Plan document.\\
			&& \tabitem Generate Test Suite.\\
			\midrule
			System Tester & Szostek B. & Runs the tests. Duties:\\
			&& \tabitem Execute tests.\\
			&& \tabitem Log results.\\
			&& \tabitem Recover from errors.\\
			&& \tabitem Document defects.\\
			\midrule
			Designer      & Szostek B. & Identifies and defines the operations, attributes, and associations of the test classes. Duties:\\
			&& \tabitem Identifies and defines the test classes.\\
			&& \tabitem Identifies and defines the test fixtures.\\
			\midrule
			Developer   & Szostek B. & 
			Develop the system/application
			Implements the application and unit tests, writes the test classes and test fixtures. Duties:\\
			&& \tabitem Develops the test classes implemented in the Test Suite.\\
			&& \tabitem Conducts unit, regression and integration testing.\\
			&& \tabitem Supports user acceptance and system testing.\\
			\bottomrule
		\end{tabular}
	\end{table}

\subsection{Tools, techniques, methods, and metrics}
	All Test Plan documents should fulfil IEEE Standard 829-2008 [1]. Software should be tested on a platform that fulfils minimal hardware and system requirements specified in SRS. Code will be developed using Microsoft Visual Studio 2015, as well as unit tests. Application is written in \Csharp language; test environment will be based on \gls{nunit} framework in version 3.0 or higher.